extends Resource

# dialog.gd

class_name Dialog

# NPC image
@export var avatar_texture: Texture
# Where are we in the dialog
@export var step: int
# NPC says
@export var dialog_slides: Array[String]
# Player have three posible answers: positive, negative and "close_dialog"
# if left empty, the dialog manager wil not create a button for them
@export var player_dialog_pos: Array[String]
@export var player_dialog_neg: Array[String]
@export var player_dialog_clr: Array[String]
# Player-answer corrosponds to the next set of dialog options
@export var pos_react: Array[int]
@export var neg_react: Array[int]
@export var clr_react: Array[int]
# If an answer needs an item and how many
@export var item_name: Array[String]
@export var item_quantity: Array[int]
# Reward for quests from npc
@export var reward_name: Array[String]
@export var reward_quantity: Array[int]
