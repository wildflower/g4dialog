extends Control

# dialog_manager.gd
# 
# fill out dialog_ui with info from dialogs

@export var _dialog_text_path: NodePath
@export var _avatar_path: NodePath
@export var _current_dialog: Dialog

var npc_name = ""

var _current_slide_index := 0

@export var game_pause := true

@onready var dialog_ui := $"."
@onready var hud := $"../Hud"

@onready var name_label = $Panel/NameLabel
@onready var but1 = $Panel/Button1
@onready var but2 = $Panel/Button2
@onready var but3 = $Panel/Button3

@onready var player := $"../Player"

@onready var _dialog_text: Label = get_node(_dialog_text_path)
@onready var _avatar: TextureRect = get_node(_avatar_path)

func _ready():
	GameEvents.connect("dialog_initiated", _on_dialog_initiated)

func _on_dialog_initiated(cur_npc) -> void:
	hud.visible = false
	npc_name = cur_npc
	name_label.text = npc_name
	_current_dialog = load("res://dialog/%s.tres" % cur_npc)
	_current_slide_index = _current_dialog.step
	
	if npc_name == "Ana":
		var dialog_var = get_node("/root/DialogVars")
		_current_slide_index = dialog_var.ana_step
	if npc_name == "Bob":
		var dialog_var = get_node("/root/DialogVars")
		_current_slide_index = dialog_var.bob_step
	
	if  game_pause:
		get_tree().paused = true
		game_pause = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		dialog_ui.visible = true
	else:
		get_tree().paused = false
		game_pause = true
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		dialog_ui.visible = false
	
	_avatar.texture = _current_dialog.avatar_texture
	show_slide()

func show_slide() -> void:
	_dialog_text.text = _current_dialog.dialog_slides[_current_slide_index]
	
	# positive dialog: giving items, accepting quests and other acknowledgments
	# do we need to have an item in our inventory?
	if _current_dialog.item_quantity[_current_slide_index] > 0:
		if player.inventory > 0:
			but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
		else:
			but1.text = ""
	else:
		but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
	# No text, No button
	if but1.text.length() > 0:
		but1.visible = true
	else:
		but1.visible = false
	
	# negative dialog like insults, refusals and sutch
	# do we need to have an item in our inventory?
	if _current_dialog.item_quantity[_current_slide_index] > 0:
		if player.inventory > 0:
			but1.text = _current_dialog.player_dialog_pos[_current_slide_index]
		else:
			but1.text = ""
	else:
		but2.text = _current_dialog.player_dialog_neg[_current_slide_index]
	# No text, No button
	if but2.text.length() > 0:
		but2.visible = true
	else:
		but2.visible = false
	
	# close/clear dialog
	but3.text = _current_dialog.player_dialog_clr[_current_slide_index]
	# No text, No button
	if but3.text.length() > 0:
		but3.visible = true
	else:
		but3.visible = false

# this button is used for positive messages that lead to other questions
func _on_button_1_pressed():
	# do we need to give an item
	if _current_dialog.item_name[_current_slide_index] != "":
		var i_name = _current_dialog.item_name[_current_slide_index]
		var i_quantity = _current_dialog.item_quantity[_current_slide_index]
		player.inventory -= i_quantity
	# Reward
	if _current_dialog.reward_name[_current_slide_index] != "":
		var r_name = _current_dialog.reward_name[_current_slide_index]
		var r_quantity = _current_dialog.reward_quantity[_current_slide_index]
		player.inventory += r_quantity
	_current_slide_index = _current_dialog.pos_react[_current_slide_index]
	show_slide()

# this button is used for negative messages that lead to other questions
func _on_button_2_pressed():
	_current_slide_index = _current_dialog.neg_react[_current_slide_index]
	show_slide()

func _on_button_3_pressed():
	_current_dialog.step = _current_dialog.clr_react[_current_slide_index]
	# save step for each npc
	var dialog_var = get_node("/root/DialogVars")
	if npc_name == "Ana":
		dialog_var.ana_step = _current_dialog.step
	if npc_name == "Bob":
		dialog_var.bob_step = _current_dialog.step
	
	get_tree().paused = false
	game_pause = true
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	dialog_ui.visible = false
	hud.visible = true

