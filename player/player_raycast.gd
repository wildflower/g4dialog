extends RayCast3D

# player_raycast.gd
#
# looking at things and npcs changes visibility for pickup and interact-label
# E-key picks up items and activates npc-dialog

@onready var pickuplabel := get_node("/root/MainScene/Hud/PickupLabel")
@onready var interactlabel := get_node("/root/MainScene/Hud/InteractLabel")

# perhaps not the best way to get this...
@onready var player := get_parent().get_parent().get_parent()

func _ready():
	pass # Replace with function body.

func _process(_delta):
	# pointing at an item/npc
	var cursor_object = self.get_collider()
	if cursor_object == null:
		pass
		pickuplabel.visible = false
		interactlabel.visible = false
	else:
		if cursor_object.is_in_group("pickup"):
			pickuplabel.visible = true
		else:
			pickuplabel.visible = false
		if cursor_object.is_in_group("npc"):
			interactlabel.visible = true
		else:
			interactlabel.visible = false

func _input(_event):
	if Input.is_key_pressed(KEY_E):
		if is_colliding():
			if get_collider() is StaticBody3D:
				var obj = get_collider()
				if obj.is_in_group("pickup"):
					# remove object from scene
					obj.queue_free()
					# add to inventory
					player.inventory += 1
				if obj.is_in_group("npc"):
					var cur_npc = obj.name
					GameEvents.emit_signal("dialog_initiated", cur_npc)
