## A basic dialog for Godot 4 using resources

https://www.youtube.com/watch?v=Xutz5dFZmKQ

Every npc is repesented in the game as an object with StaticBody/Collision/Mesh, as well as their own dialog-resource file.
When pointing at an npc and using the E key the dialog_ui becomes visible and are populated with data from the active npc-dialog-resource.

Pointing at a green box and using the E key picks up the box and adds it to the player-inventory.


The Dialog class holds the following information on every npc:

- avatar_texture: NPC image
- step: Where are we in the dialog
- dialog_slides: NPC says  
Player have three posible answers: positive, negative and "close_dialog"  
if left empty, the dialog manager wil not create a button for them  
- player_dialog_pos: A positive sentence from player
- player_dialog_neg: A positive sentence from player
- player_dialog_clr: A sentence the closes the dialog  
Player-answer corrosponds to the next set of dialog options  
- pos_react: What to show next if the pos sentence is clicked
- neg_react: What to show next if the neg sentence is clicked
- clr_react: What to show next time the dialog opens  
If an answer needs an item and how many  
- item_name: i.e. green_box or similar
- item_quantity: how many do we need  
Reward for quests from npc  
- reward_name: i.e. coin or similar
- reward_quantity: how many do we get  


Its relatively easy to add more dialog options, like a "player_dialog_insult" for a insult sentence from player, or another quest requirement like clearing an area or killing a monster.
Another thing to add could be a "mood" like in the morrowind dialog, where a negative answer makes the npc like you less and vice versa.

Select /dialog/Bob.tres inside Godot to edit Bob's dialog in the inspector.

Don't know how intuitive this system is from an outside perspective, but if anyone need it, I can make a quick "howto" on youtube.
